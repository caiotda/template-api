const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

const validationRegex = {
  "name":/([A-zÀ-ú][ ]?)+/,
  "email": /\S+@\S+\.\S+/,
  "password": /\w{8,32}/,
  "passwordConfirmation": /\w{8,32}/,
}


const isCorrectlyFormatted = (field, value) => {
  return validationRegex[field].test(value);
}


const validadeRequisitionBody = (body) => {
  const reqParams = ["name", "email", "password", "passwordConfirmation"]

  for (const fieldName of reqParams) {
    if ( !(fieldName in body) ) {
      return [`Request body had missing field ${fieldName}`, 400];
    }
    else {
      if ( !isCorrectlyFormatted(fieldName, body[fieldName]) ) {
        return [`Request body had malformed field ${fieldName}`, 400];
      }
    }
  }

  if (body["password"] != body["passwordConfirmation"]) {
    return ["Password confirmation did not match", 422];
  }
  return ["Ok", 201];
}
module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  api.post("/users", ({body}, res) => {
    [error, httpCode] = validadeRequisitionBody(body);

    const entityId = uuid();
    const {name, email, password} = body;

    if (httpCode != 201) {
      res.status(httpCode).send({ error });
    }
    else {
      stanConn.publish('users',
      `{
          "eventType": "UserCreated",
          "entityId": "${entityId}",
          "entityAggregate": {
            "name": "${name}",
            "email": "${email}",
            "password": "${password}"
          }
      }`, (err, guid) => {
        if (err) {
          console.log('publish failed: ' + err)
        } else {
          console.log('published message with guid: ' + guid)
        }
      })
      mongoClient.db("pingr").collection("users").findOne({ entityId })
      res.status(httpCode).send(
        {
          "user": {
            "id": entityId,
            "name": name,
            "email": email
          }
        });
    }
  });

  api.delete("/users/:uuid", (req, res) => {
    let { authorization } = req.headers
    const userid = req.params.uuid
    if (!authorization) {
      res.status(401).send({ error: "Access token not found" })
    }
    else {
      authorization = authorization.split(" ")[1]
      const decoded = jwt.verify(authorization, secret)
      if (decoded.id == userid) {
        res.status(200).send({ id: userid })
        stanConn.publish('users',
        `{
            "eventType": "UserDeleted",
            "entityId": "${userid}",
            "entityAggregate": {}
        }`, (err, guid) => {
        if (err) {
          console.log('publish failed: ' + err)
        } else {
          console.log('published message with guid: ' + guid)
        }
      })
      }
      else {
        res.status(403).send({ error: "Access token did not match user id" })
      }
    }
  })
  /* ******************* */

  return api;
};
