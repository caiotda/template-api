const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subscription = conn.subscribe('users', opts);

    subscription.on('message', (msg) => {
      const msgData = msg.getData()
      const jsonData = JSON.parse(msgData)
      console.log(jsonData)
  })

    /* ******************* */
  });

  return conn;
};
